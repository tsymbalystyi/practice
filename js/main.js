var app = new Vue({
    el:'#app',

    data: {
        products:[
            {id: 1,title: 'Fuerte', short_text: 'Pear-shaped, small stone, droplet shape ', image: 'Fuerte.jpg', desc:''},
            {id: 2,title: 'Pinkerton', short_text: 'Elongated fruit, small stone, thick pimply skin. ', image: 'Pinkerton.jpg', desc:''},
            {id: 3,title: 'Ettinger', short_text: 'Medium-sized fruits, oval-pear-shaped, large stone, thin smooth peel.', image: 'Ettinger.jpg', desc:''},
            {id: 4,title: 'Haas', short_text: 'Oval fruits, small stone, The rind is very dense, pimpled, dark purple.', image: 'Haas.jpg', desc:''},
            {id: 5,title: 'Bacon', short_text: 'Fruits are small, medium-sized stone, very thin and smooth green peel.', image: 'Bacon.jpg', desc:''}
        ],

        product:[],

        btnVisible: 0,

        cart: [],

        contactFields:[],

        formVisible: 1,

        capitalsUrl: 'https://restcountries.eu/rest/v2/regionalbloc/eu',

        capitals: [],


    },

    methods: {
        getProduct(){
            if(window.location.hash){
                var id = window.location.hash.replace('#', '');
                if(this.products && this.products.length>0){
                    for(i in this.products){
                        if(this.products[i] && this.products[i].id && id==this.products[i].id)
                            this.product = this.products[i];
                    }
                }
            }
        } ,

        addToCart(id){
            var cart = [];
            if(window.localStorage.getItem('cart')){
                cart = window.localStorage.getItem('cart').split(',');
            }

            if(cart.indexOf(String(id))==-1){
                cart.push(id);
                window.localStorage.setItem('cart', cart.join());
                this.btnVisible = 1;
            }
        },

        checkInCart(){
            if(this.product && this.product.id && window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id))!=-1)
            this.btnVisible=1;
        },

        getCart(){
            let localCart = window.localStorage.getItem('cart').split(',')
            for(i of this.products){
                
                if(localCart.indexOf(String(i.id)) != -1){
                    this.cart.push(i);
                }
            }
        },

        removeFromCart(id){
           this.cart = this.cart.filter(product => product.id != id);

           window.localStorage.setItem('cart', this.cart.join());
        },

        makeOrder(){
            this.formVisible = 0;
            this.cart = []
            window.localStorage.clear
        },

        sendRequest() {
            fetch(this.capitalsUrl)
                .then(result => result.json())
                .then(data => {
                    for (country of data){
                        this.capitals.push(country.capital)
                    }
                })
        }

        
    },

    mounted() {
        this.getProduct();
        this.checkInCart();
        this.getCart();
        
    },

    created() {
        this.sendRequest();
    },
})

